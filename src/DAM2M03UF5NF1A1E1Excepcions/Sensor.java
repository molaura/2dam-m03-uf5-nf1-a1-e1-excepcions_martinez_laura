
package DAM2M03UF5NF1A1E1Excepcions;

import java.util.Random;

/**
 *
 * @author Laura
 */
public class Sensor {

    public Sensor() {
    }

    public int obtenirAlcada() {

        Random alcadaAleatoria = new Random();
        int novaAlcada = alcadaAleatoria.nextInt((215 - 130) + 1) + 130;

        return novaAlcada;
    }
}
