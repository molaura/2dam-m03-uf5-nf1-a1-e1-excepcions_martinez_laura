package DAM2M03UF5NF1A1E1Excepcions;


/**
 *
 * @author Laura
 */
public class PortaEntrada {

    private final static int MASSA_ALT = 190;
    private final static int MASSA_BAIX = 150;

    public PortaEntrada() {
    }

    public void comprovar(int alcada) throws AlcadaException {

        if (alcada < MASSA_BAIX) {
            throw new PocaAlcadaException("ERROR: no arriba a l'alçada mínima de 150");
        } else if (alcada > MASSA_ALT) {
            throw new MassaAlcadaException("ERROR: alçada màxima de 190 sobrepassada");
        }
        if (alcada > MASSA_BAIX && alcada < MASSA_ALT) {
            System.out.println("Obrint la porta a l'alçada " + alcada);
        }

    }
}
