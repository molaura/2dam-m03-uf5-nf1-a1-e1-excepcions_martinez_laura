package DAM2M03UF5NF1A1E1Excepcions;

/**
 * Classe que crea una excepcio.
 * Hereta de AlcadaException.
 * On sigui cridada es llançara el missatge que correspongui a aquesta excepció
 */

/**
 *
 * @author Laura
 */
public class PocaAlcadaException extends AlcadaException {

    public PocaAlcadaException() {
        super();
    }

    public PocaAlcadaException(String missatge) {
        super(missatge);
    }

}
