package DAM2M03UF5NF1A1E1Excepcions;

/**
 *
 * @author Laura
 */
public class AtraccioFires {

    public static void main(String[] args) {
        int[] persones = new int[20];

        Sensor sensor1;
        PortaEntrada sensor2;

        for (int i = 0; i < persones.length; i++) {

            sensor1 = new Sensor();
            int alcada = sensor1.obtenirAlcada();
            System.out.println("SENSOR: Alçada llegida = " + alcada);
            sensor2 = new PortaEntrada();

            try {
                sensor2.comprovar(alcada);
            } catch (AlcadaException ex) {
                System.out.println(ex.getMessage());
            }

        }

    }
}
