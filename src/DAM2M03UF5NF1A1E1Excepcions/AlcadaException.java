package DAM2M03UF5NF1A1E1Excepcions;

/**
 * Aquesta classe hereta de la Exception. 
 * És el primer pas per a crear excepcions pròpies.
 * A partir d'aquesta s'haurà d'implementar les classes necessàries en cada cas que hereteran d'ella.
 */


/**
 *
 * @author Laura
 */
public class AlcadaException extends Exception{
    
    public AlcadaException(){
    super();
    }
    
    public AlcadaException(String missatge){
        super(missatge);
    } 
}
